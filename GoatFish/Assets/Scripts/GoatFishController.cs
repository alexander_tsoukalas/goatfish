﻿using UnityEngine;
using System.Collections;

public class GoatFishController : MonoBehaviour {

	// Use this for initialization

    float x=0;

    float y=0;

    public float dir=1;

    Transform gfx;

    public float lastVel;

    public GameObject projectile;

    Transform fpoint;

	void Awake () {
        
        gfx = transform.FindChild("GFX");

        fpoint = gfx.FindChild("fpoint");

        gfx.localScale = new Vector3(1, 1, 1);
	}
	
	// Update is called once per frame
	void Update () {

        x = Input.GetAxis("Horizontal");

        y = Input.GetAxis("Vertical");


        if(x>0.1f)
        {
            dir = 1;
        }
        else if(x<-0.1f)
        {
            dir = -1;
        }

        gfx.localScale = new Vector3(dir, 1, 1);
        

        if(Input.GetButtonDown("Fire1"))
        {
            GameObject g=Instantiate(projectile, fpoint.position, Quaternion.identity) as GameObject;

            g.GetComponent<Rigidbody2D>().AddForce(dir*Vector2.right * 25, ForceMode2D.Impulse);

        }

	
	}


    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(x, y) * 6);


        Vector2 speed = GetComponent<Rigidbody2D>().velocity;


        speed.x = Mathf.Clamp(speed.x, -20, 20);
        speed.y = Mathf.Clamp(speed.y, -20, 20);

        lastVel = speed.x;
    }

}
