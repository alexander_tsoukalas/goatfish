﻿using UnityEngine;
using System.Collections;

public class Horns : MonoBehaviour {

	// Use this for initialization


    GoatFishController m;

	void Awake () {

        m = GetComponentInParent<GoatFishController>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D d)
    {
        if (Mathf.Abs(m.lastVel) > 15)
        {
            if (d.CompareTag("B.Rock"))
            {
                Destroy(d.gameObject);
            }
        }
    }

}
