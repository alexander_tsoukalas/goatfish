﻿using UnityEngine;
using System.Collections;

public class BubbleScript : MonoBehaviour {

	
    // Use this for initialization

    Transform gfx;
    bool splash = false;
    ParticleSystem p;

    float alive=0;

    bool timeUp=false;

    void Awake () {

        p = GetComponentInChildren<ParticleSystem>();

        gfx = transform.Find("Sphere");
	}
	
	// Update is called once per frame
	void Update () {

        alive += Time.deltaTime;


        if (!timeUp)
        {
            if (alive >= 2f)
            {
                gfx.gameObject.SetActive(false);
                p.Play();
                timeUp = true;
                
            }

        }
        else
        {
            if(p.isStopped)
            {
                Destroy(gameObject);
            }
        }

        if(splash)
        {
            if(p.isStopped)
            {
                Destroy(gameObject);
            }
        }



	}


    void OnCollisionEnter2D(Collision2D d)
    {
        Debug.Log("BUBBLE HIT");

        p.Play();
        gfx.gameObject.SetActive(false);

        
        splash=true;
    
    }
}
